#include "Image.h"
#include "Math.h"
#include "Util.h"

#include <fstream>
#include <iostream>
#include <cmath>

Image::Image(std::string filename) {
    loadPpm(filename);
}

Image::Image(int height, int width) : height(height), width(width) {
    buffer = new Vector[height * width];

}

void Image::saveAsPpm(std::string filename, int max_val) {
    std::ofstream file(filename);
    file << "P3 " << width << " " << height << " " << max_val << "\n";
    for(int i = 0; i < height; i ++) {
        for(int j = 0; j < width; j ++) {
            for(int k = 0; k < 3; k ++) {
                int x = max_val * pow(buffer[i * width + j][k], 1/2.2);
                file << std::min(std::max(x, 0), max_val) << " ";
            }
        }
        file << "\n";
    }
}

Image* Image::loadPpm(std::string filename) {
    std::cout << "Starting to load " << filename << std::endl;
    filename = std::string("images/") + filename + ".ppm";
    std::ifstream file(filename);
    if(!file.good()) return nullptr;
    double x, maxval;
    int width, height;
    std::string buf;
    file >> buf >> width >> height >> maxval;
    Image* image = new Image(height, width);
    image->buffer = new Vector[width * height];
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++)
    for(int k = 0; k < 3; k ++) {
        file >> x;
        image->buffer[i * width + j][k] = x / maxval;
    }
    return image;
}

void Image::loadPpmRaw(std::string filename) {
    std::ifstream file(filename);
    char x;
    std::string buf;
    double maxval;
    file >> buf >> width >> height >> maxval;
    buffer = new Vector[width * height];
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++)
    for(int k = 0; k < 3; k ++) {
        file >> x;
        buffer[0 * width + j][k] = int(uint8_t(x));
    }
}

void Image::spread(int i, int j, double spreadRate, double spreadChance) {
    spreadChance *= 0.9;
    Vector p = getPixel(i, j);
    if(rand() / double(RAND_MAX) < spreadChance) {
        setPixel(i - 1, j, p, spreadRate);
        spread(i - 1, j, spreadRate, spreadChance);
    }
    if(rand() / double(RAND_MAX) < spreadChance) {
        setPixel(i + 1, j, p, spreadRate);
        spread(i + 1, j, spreadRate, spreadChance);
    }
    if(rand() / double(RAND_MAX) < spreadChance) {
        setPixel(i, j - 1, p, spreadRate);
        spread(i, j - 1, spreadRate, spreadChance);
    }
    if(rand() / double(RAND_MAX) < spreadChance) {
        setPixel(i, j + 1, p, spreadRate);
        spread(i, j + 1, spreadRate, spreadChance);
    }
}

Vector Image::getPixel(const Vector& p) const{
    //print(p);
    int i = round(p[1] * (height - 1));
    int j = round(p[2] * (width - 1));
    int k = int(p[0]) * unit;
    if(i < 0 || i >= height || j < 0 || j >= width) {
        p.print();
        throw std::runtime_error("i and j for pixel not coorect");
    }
    return buffer[i * width + j + k];
}

Vector Image::getPixel(int i, int j, Vector default_val) const{
    if(i < 0 || i >= height || j < 0 || j >= width) return default_val;
    return buffer[i * width + j];
}

void Image::setPixel(int i, int j, const Vector& p) {
    if(i < 0 || i >= height || j < 0 || j >= width) return;
    buffer[i * width + j] = p;
}

void Image::setPixel(int i, int j, const Vector& p, double percent) {
    if(i < 0 || i >= height || j < 0 || j >= width) return;
    buffer[i * width + j] *= 1 - percent;
    buffer[i * width + j] += p * percent;
}

Image* Image::randomSmooth(int height, int width, double changeRate) {
    Image* image = new Image(height, width);
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++) {
        Vector c = Vector::random(changeRate);
        if(i > 0) {
            if(j > 0) {
                c += image->buffer[(i - 1) * width + j];
                c += image->buffer[i * width + j - 1];
                c /= 2;
            } else c += image->buffer[(i - 1) * width + j];
        } else {
            if(j > 0) c += image->buffer[i * width + j - 1];
            else c = Vector::random();
        }
        c.compWiseMax(Vector(0, 0, 0));
        c.compWiseMin(Vector(1, 1, 1));
        image->buffer[i * width + j] = c;
    }
    return image;
}

Image* Image::randomSpread(int height, int width, double spreadRate, double spreadChance) {
    Image* image = new Image(height, width);
    for(int k = 0; k < height * width / 100; k ++) {
        int i = rand() % height;
        int j = rand() % width;
        image->setPixel(i, j, Vector::random());
        image->spread(i, j, spreadRate, spreadChance);
    }
    return image;
}

void Image::rescale(double maxv) {
    double mm = 0;
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++) {
        mm = std::max(mm, getPixel(i,j).norm());
    }
    mm /= maxv;
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++) {
        setPixel(i, j, getPixel(i, j) / mm);
    }
}

void Image::compressX() {
    std::vector<Matrix> kernels;
    kernels.push_back(Matrix(-1, 0, 1,
                            -2, 0, 2,
                            -1, 0, 1));
    kernels.push_back(Matrix(1, 2, 1,
                            0, 0, 0,
                            -1, -2, -1));

    Image* E = new Image(height, width);
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++) {
        Vector v(0, 0, 0);
        for(Matrix kernel : kernels)
        for(int ii = -1; ii < 2; ii ++)
        for(int jj = -1; jj < 2; jj ++) {
            v += getPixel(i + ii, j + jj) * kernel.getValue(ii + 1, jj + 1);
        }
        E->setPixel(i, j, Vector(1, 1, 1) * v.norm2());
    }
    Image* V = new Image(height, width);
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++) {
        Vector v = compWiseMin(compWiseMin(V->getPixel(i-1, j-1), V->getPixel(i-1, j)), V->getPixel(i-1, j+1))
        + E->getPixel(i, j);
        V->setPixel(i, j, v);
    }
    int* minI = new int[height];
    double p = MAXFLOAT;
    for(int i = 0; i < width; i ++) {
        if(V->getPixel(i, height-1)[0] < p){
            p = V->getPixel(i, height-1)[0];
            minI[height-1] = i;
        }
    }
    int pI = minI[height-1];
    for(int i = height - 2; i >= 0; i --){
        if(pI == 0) {
            if(V->getPixel(i, pI)[0] > V->getPixel(i, pI+1)[0]) pI ++;
        } else if(pI == width-1) {
            if(V->getPixel(i, pI)[0] > V->getPixel(i, pI-1)[0]) pI --;
        } else {
            if(V->getPixel(i, pI-1)[0] > V->getPixel(i, pI+1)[0]) {
                if(V->getPixel(i, pI+1)[0] < V->getPixel(i, pI)[0]) pI ++;
            } else {
                if(V->getPixel(i, pI-1)[0] < V->getPixel(i, pI)[0]) pI --;
            }
        }
        minI[i] = pI;
    }
    Image* nimage = new Image(height, width-1);
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++) {
        if(j > minI[i]) {
            nimage->setPixel(i, j - 1, getPixel(i, j));
        } else if (j < minI[i]) {
            nimage->setPixel(i, j, getPixel(i, j));
        }
    }
    buffer = nimage->buffer;
    width --;

    //for(int i = 0; i < height; i ++)
    //for(int j = 0; j < width; j ++) {
        //if(j == minI[i]) V->setPixel(i, j, Vector());
    //}
    //V->rescale();
    //buffer = V->buffer;
}

#include "Util.h"
#include "Image.h"
#include "Math.h"

#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <vector>

#define RAND (uniform(engine))

double reflectedProportion(double n1, double n2, double proj) {
    double diff = n1 - n2;
    double summ = n1 + n2;
    double k = diff * diff / (summ * summ);
    double K = 1 - proj;
    return k + (1 - k) * K * K * K * K * K; 
}

double absd(double r) {
    return (r > 0) ? r : -r;
}
 
Vector absv(const Vector& a) {
    return Vector(absd(a[0]), absd(a[1]), absd(a[2]));
}

int binomial(int n, double p) {
    int k = 0;
    for(int i = 0; i < n; i ++) 
        if(RAND < p) k ++;
    return k;
}
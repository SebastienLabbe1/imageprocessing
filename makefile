CC=g++
TARGET=prog
FLAGS=-std=c++1z -Wall -fopenmp -O3
LINKS=

CODEDIR=code
OBJDIR=objects
HEADERDIR=headers

MAIN=main.cpp
OBJNAMES=Image.o Math.o Util.o

SOURCE=$(MAIN)
INCLUDE=-I./$(HEADERDIR)
OBJ=$(OBJNAMES:%=$(OBJDIR)/%)

TRIES=1


all: $(SOURCE) $(OBJ)
	$(CC) $(FLAGS) $(INCLUDE) -o $(TARGET) $(SOURCE) $(OBJ) $(LINKS)

$(OBJDIR)/%.o: $(CODEDIR)/%.cpp $(HEADERDIR)/%.h
	$(CC) $(FLAGS) $(INCLUDE) -c -o $@ $< 

clean:
	rm $(OBJDIR)/*.o prog -f

force: clean all

run: all
	time ./prog $(TRIES)
	xdg-open result.ppm

#include "Image.h"
#include "Math.h"

#include <sstream>

int main() {
    Image* image = Image::loadPpm("lake_house");
    std::stringstream ss;
    ss << time(0);
    std::string ts = ss.str();
    for(int i = 0; i < 400; i ++) {
        image->compressX();
        image->saveAsPpm(std::string("results/result") + ts + "_" + std::to_string(i) + ".ppm");
    }
    image->saveAsPpm("result.ppm");
    
    return 0;
}

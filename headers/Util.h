#ifndef UTIL_H
#define UTIL_H

#include "Math.h"

#include <string>

double reflectedProportion(double n1, double n2, double proj);

double absd(double r);

Vector absv(const Vector& r);

int binomial(int n, double p);

#endif // UTIL_H
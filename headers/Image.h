#ifndef IMAGE_H
#define IMAGE_H

#include <string>

#include "Math.h"

class Image {
    public:
        Vector* buffer = nullptr;
        int height, width, unit;

        Image(std::string filename);
        Image(int height, int width);

        void saveAsPpm(std::string filename, int max_val = 255);
        void loadPpmRaw(std::string filename);
        //void loadPpm(std::string filename);
        void spread(int i, int j, double spreadRate, double spreadChance);

        Vector getPixel(const Vector& p) const;
        Vector getPixel(int i, int j, Vector default_val = Vector()) const;
        void setPixel(int i, int j, const Vector& p);
        void setPixel(int i, int j, const Vector& p, double percent);
        void compressX();
        void rescale(double maxv = 1.0);

        static Image* randomSmooth(int height, int width, double changeRate);
        static Image* randomSpread(int height, int width, double spreadRate, double spreadChance);
        static Image* loadPpm(std::string filename);
};

#endif // IMAGE_H
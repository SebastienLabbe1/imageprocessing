#ifndef MATH_H
#define MATH_H

#include <random>

static std::default_random_engine engine(10);
static std::uniform_real_distribution<double> uniform(1, 0);

class Vector {
	private:
		double values[3];

	public:
    	Vector(double x = 0, double y = 0, double z = 0);

		const double& operator[] (int i) const;
		double& operator[] (int i);
		Vector& operator+= (const Vector& vec);
		Vector& operator-= (const Vector& vec);
		Vector& operator*= (double r);
		Vector& operator/= (double r);

		double norm () const;
		double norm2 () const;
		void normalize ();
		Vector normalized() const;
		Vector getOrthogonal() const;

		void copy(const Vector& vec);
		void clear();

		Vector& compWiseMin(const Vector& vec);
		Vector& compWiseMax(const Vector& vec);
		Vector& compWiseMult(const Vector& vec);

		double min() const ;
		double minAbs() const;
		double max() const;
		int minI() const;
		int minAbsI() const;
		int maxI() const;

		Vector randomCos() const;
		Vector randomPow(double alpha, double& costeta) const;

		Vector reflect(const Vector& normal) const;
		Vector refract(const Vector& normal, double n1, double n2) const;

		Vector reflect(const Vector& normal, double proj) const;
		Vector refract(const Vector& normal, double proj, double n1, double& n2) const;
		
		bool hasnan() const;
		void print() const;

		static Vector random(double r);
		static Vector random();
};

Vector operator+ (const Vector& a, const Vector& b);
Vector operator- (const Vector& a, const Vector& b);
Vector operator* (const Vector& a, double r);
Vector operator/ (const Vector& a, double r);

Vector compWiseMin(const Vector& a, const Vector& b);
Vector compWiseMax(const Vector& a, const Vector& b);
Vector compWiseMult(const Vector& a, const Vector& b);

double dot (const Vector& a, const Vector& b);
double dotPos (const Vector& a, const Vector& b);
Vector cross (const Vector& a, const Vector& b);
double cosTeta(const Vector& a, const Vector& b);
Vector barycenterValue(const Vector& x, const Vector& y, const Vector& z, double u, double v);
Vector rotateX(const Vector& a, double teta);

class Matrix {
    public:
        double values[9];

        Matrix(double x00 = 0., double x01 = 0., double x02 = 0., 
			double x10 = 0., double x11 = 0., double x12 = 0., 
			double x20 = 0., double x21 = 0., double x22 = 0.);

        Matrix(double* x);

		double getValue(int i, int j);
};

#endif // MATH_H
